.PHONY: all release clean
all: trains.exe
release: TrainsPaste.zip

TrainsPaste.zip: TrainsPaste
	7z a TrainsPaste.zip TrainsPaste
	rm -rf TrainsPaste plug-ins trains.exe

TrainsPaste: trains.exe
	mkdir TrainsPaste
	cp -rf \
		trains.exe \
		trains.nako \
		config.ini \
		Makefile \
		LICENSE \
		plug-ins \
		TrainsPaste

trains.exe: trains.nako
	nakomake trains.nako -t vnako -p dnako,nakofile,nakoctrl

clean:
	rm -rf trains.exe plug-ins TrainsPaste TrainsPaste.zip
